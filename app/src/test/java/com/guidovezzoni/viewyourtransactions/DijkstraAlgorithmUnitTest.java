package com.guidovezzoni.viewyourtransactions;

import com.guidovezzoni.viewyourtransactions.algorithm.DijkstraAlgorithm;
import com.guidovezzoni.viewyourtransactions.algorithm.Edge;
import com.guidovezzoni.viewyourtransactions.algorithm.Graph;
import com.guidovezzoni.viewyourtransactions.algorithm.Vertex;

import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by guido on 30/07/16.
 */
public class DijkstraAlgorithmUnitTest {

    private List<Vertex> vertices;
    private List<Edge> edges;

    @Test
    public void testExcute() {
        vertices = new ArrayList<Vertex>();
        edges = new ArrayList<Edge>();
        for (int i = 0; i < 11; i++) {
            Vertex location = new Vertex("Node_" + i, "Node_" + i);
            vertices.add(location);
        }

        addLane("Edge_0", 0, 1, 85);
        addLane("Edge_1", 0, 2, 217);
        addLane("Edge_2", 0, 4, 173);
        addLane("Edge_3", 2, 6, 186);
        addLane("Edge_4", 2, 7, 103);
        addLane("Edge_5", 3, 7, 183);
        addLane("Edge_6", 5, 8, 250);
        addLane("Edge_7", 8, 9, 84);
        addLane("Edge_8", 7, 9, 167);
        addLane("Edge_9", 4, 9, 502);
        addLane("Edge_10", 9, 10, 40);
        addLane("Edge_11", 1, 10, 600);

        // Lets check from location Loc_1 to Loc_10
        Graph graph = new Graph(vertices, edges);
        DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(graph);
        dijkstra.execute(vertices.get(0));
        LinkedList<Vertex> path = dijkstra.getPath(vertices.get(10));

        assertNotNull(path);
        assertTrue(path.size() > 0);

        for (Vertex vertex : path) {
            System.out.println(vertex);
        }

    }

    private void addLane(String laneId, int sourceLocNo, int destLocNo,
                         int duration) {
        Edge lane = new Edge(laneId, vertices.get(sourceLocNo), vertices.get(destLocNo), duration);
        edges.add(lane);
    }

    @Test
    public void testExcuteCurrency() {
        vertices = new ArrayList<Vertex>();
        vertices.add(new Vertex("GBP", "GBP"));  // 0
        vertices.add(new Vertex("EUR", "EUR"));  // 1
        vertices.add(new Vertex("USD", "USD"));  // 2
        vertices.add(new Vertex("CAD", "CAD"));  // 3
        vertices.add(new Vertex("AUD", "AUD"));  // 4

        edges = new ArrayList<Edge>();
        edges.add(new Edge("GBPEUR", vertices.get(0), vertices.get(1), 1));
        edges.add(new Edge("EURUSD", vertices.get(1), vertices.get(2), 1));
        edges.add(new Edge("GBPCAD", vertices.get(0), vertices.get(3), 1));
        edges.add(new Edge("CADAUD", vertices.get(3), vertices.get(4), 1));
        edges.add(new Edge("AUDUSD", vertices.get(4), vertices.get(2), 1));

        // path 1 GBP EUR USD
        // path 2 GBP CAD AUD USD

        Graph graph = new Graph(vertices, edges);
        DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(graph);


        dijkstra.execute(vertices.get(0));
        LinkedList<Vertex> path = dijkstra.getPath(vertices.get(2));

        assertNotNull(path);
        assertTrue(path.size() > 0);

        for (Vertex vertex : path) {
            System.out.println(vertex);
        }

    }

    @Test
    public void testExcuteCurrency2() {
        Graph graph = new Graph();
        graph.addEdge("GBP", "EUR");
        graph.addEdge("EUR", "USD");
        graph.addEdge("GBP", "CAD");
        graph.addEdge("CAD", "AUD");
        graph.addEdge("AUD", "USD");
        graph.addEdge("AUD", "X01");
        graph.addEdge("X01", "X02");

        // path 1 GBP EUR USD
        // path 2 GBP CAD AUD USD
        // path 3 GBP CAD AUD X01 X02


        DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(graph);
        dijkstra.execute(graph.getVertex("GBP"));

        LinkedList<Vertex> path = dijkstra.getPath(graph.getVertex("USD"));
        assertNotNull(path);
        assertTrue(path.size() == 3);

        for (Vertex vertex : path) {
            System.out.println(vertex);
        }

        path = dijkstra.getPath(graph.getVertex("X02"));
        assertNotNull(path);
        assertTrue(path.size() == 5);

        for (Vertex vertex : path) {
            System.out.println(vertex);
        }

    }
}
