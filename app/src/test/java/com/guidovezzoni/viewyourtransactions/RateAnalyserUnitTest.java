package com.guidovezzoni.viewyourtransactions;

import com.guidovezzoni.viewyourtransactions.controller.RatesAnalyser;
import com.guidovezzoni.viewyourtransactions.models.api.RateModel;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;


/**
 * Created by guido on 30/07/16.
 */
public class RateAnalyserUnitTest {
    private RatesAnalyser mRatesAnalyser;
    private List<RateModel> mRateList;

    @Before
    public void setUp() throws Exception {
        mRateList = new ArrayList<>();
        mRateList.add(new RateModel("GBP", "EUR", 1.18));
        mRateList.add(new RateModel("EUR", "USD", 1.12));

        mRatesAnalyser = new RatesAnalyser(mRateList, true);
    }

    @Test
    public void testPreconditions() throws Exception {
        assertEquals(2, mRateList.size());
    }

    @Test
    public void testKnownRate() throws Exception {
        assertEquals(1.18, mRatesAnalyser.getRate("GBP", "EUR"), 0.005);
    }

    @Test
    public void testInverseRate() throws Exception {
        //this works ok if I use reverse rates
        assertEquals(0.85, mRatesAnalyser.getRate("EUR", "GBP"), 0.005);

        // but if I disable it, it won't work....
        mRatesAnalyser = new RatesAnalyser(mRateList, false);
        assertEquals(0, mRatesAnalyser.getRate("EUR", "GBP"), 0.005);
    }

    @Test
    public void testUnknownRate() throws Exception {
        assertEquals(1.32, mRatesAnalyser.getRate("GBP", "USD"), 0.005);
    }


    @Test
    public void testNoRates() throws Exception {
        mRateList = new ArrayList<>();
        mRatesAnalyser = new RatesAnalyser(mRateList, true);

        assertEquals(0, mRatesAnalyser.getRate("GBP", "EUR"), 0.005);
    }
}
