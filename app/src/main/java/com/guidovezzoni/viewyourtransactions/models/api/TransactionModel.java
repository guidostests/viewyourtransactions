package com.guidovezzoni.viewyourtransactions.models.api;

import org.parceler.Parcel;

/**
 * Created by guido on 30/07/16.
 */

//{"amount":"25.2","sku":"J4064","currency":"CAD"}
@Parcel
public class TransactionModel {
    public String sku;
    public Double amount;
    public String currency;

    // @Ignore this should be ignored by retrofit, json conversions, etc, but not parceler
    public Double mAmountGbp;

    // @Ignore this should be ignored by retrofit, json conversions, etc, but not parceler
    // set to true if the rate couldn't b found
    public boolean mRateZeroFlag = false;

}
