package com.guidovezzoni.viewyourtransactions.models.data;

import android.support.annotation.NonNull;

import com.guidovezzoni.viewyourtransactions.controller.RatesAnalyser;
import com.guidovezzoni.viewyourtransactions.models.api.TransactionModel;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by guido on 30/07/16.
 *
 * this class contains all the info for each sku and it's used to pooulate the details fragment
 */
@Parcel
public class SkuSummary {
    private static final String GBP_CURRENCY="GBP";

    public String mSku;
    public Double mAmountGbp;
    public List<TransactionModel> mTransactions;

    // set to true if the rate couldn't b found
    public boolean mRateZeroFlag = false;

    public SkuSummary() {
    }

    public SkuSummary(String sku, TransactionModel transaction) {
        mSku = sku;
        mAmountGbp = null;

        mTransactions = new ArrayList<>();
        if (transaction != null) {
            mTransactions.add(transaction);
        }
    }

    /*
    * iterates through the list and convert and calculates total in GBP
    * */
    public void convertsAndAddup(@NonNull RatesAnalyser ratesAnalyser) {
        Double total = 0D;

        for (TransactionModel transactionModel : mTransactions) {
            if (transactionModel.currency.equals(GBP_CURRENCY)) {
                transactionModel.mAmountGbp = transactionModel.amount;
            } else {
                Double rate = ratesAnalyser.getRate(transactionModel.currency, GBP_CURRENCY);
                if (rate != 0) {
                    transactionModel.mAmountGbp = transactionModel.amount *
                            rate;
                } else {
                    mRateZeroFlag = true;
                    transactionModel.mRateZeroFlag = true;
                    transactionModel.mAmountGbp = 0D;
                }
            }
            total += transactionModel.mAmountGbp;
        }

        mAmountGbp = total;
    }

    /*
    * add a transaction to the list
    * */
    public void addTransaction(@NonNull TransactionModel transaction) {
        mTransactions.add(transaction);
    }

    public int getTransactionCount() {
        return mTransactions.size();
    }
}
