package com.guidovezzoni.viewyourtransactions.models.api;

/**
 * Created by guido on 30/07/16.
 */

//{"from":"USD","rate":"0.77","to":"GBP"}
public class RateModel {
    public String from;
    public String to;
    public Double rate;

    public String getHashMapKey() {
        return from + to;
    }

    public RateModel() {
    }

    public RateModel(String from, String to, Double rate) {
        this.from = from;
        this.to = to;
        this.rate = rate;
    }

    public RateModel getOppositeRate() {
        return new RateModel(to, from, rate == 0 ? 0 : (1 / rate));
    }
}
