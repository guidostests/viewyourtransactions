package com.guidovezzoni.viewyourtransactions.activity;

import com.guidovezzoni.viewyourtransactions.models.data.SkuSummary;

import java.util.List;

/**
 * Created by guido on 13/08/16.
 */
public class SkuListPresenterImpl implements SkuListPresenter{
    private SkuListView mSkuListView;
    private SkuListModel mSkuListModel;

    public SkuListPresenterImpl(SkuListView skuListView) {
        this.mSkuListView = skuListView;
        this.mSkuListModel = new SkuListModelImpl(mSkuListView.getContext());
    }


    @Override
    public void onResume() {
        if (mSkuListView != null) {
            mSkuListView.showProgress();
        }

        mSkuListModel.retrieveSkus(new SkuListModel.OnSkusRetrievedListener() {
            @Override
            public void onCompletion(List<SkuSummary> skuSummaries) {
                if (mSkuListView != null) {
                    mSkuListView.setList(skuSummaries);
                    mSkuListView.hideProgress();
                }
            }
        });
    }

    @Override
    public void onItemClicked(int position, SkuSummary skuSummary) {
        if (mSkuListView != null) {
            mSkuListView.openDetailView(skuSummary);
        }

    }

    @Override
    public void onDestroy() {
        mSkuListView = null;
    }
}
