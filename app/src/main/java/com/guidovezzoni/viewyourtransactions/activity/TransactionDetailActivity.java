package com.guidovezzoni.viewyourtransactions.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.guidovezzoni.viewyourtransactions.R;
import com.guidovezzoni.viewyourtransactions.fragment.TransactionDetailFragment;
import com.guidovezzoni.viewyourtransactions.models.data.SkuSummary;

import org.parceler.Parcels;

public class TransactionDetailActivity extends AppCompatActivity {
    private static final String SKU_SUMMARY_KEY = "SKU_SUMMARY";

    public static Intent getNewIntent(Context context, SkuSummary skuSummary) {
        Intent intent = new Intent(context, TransactionDetailActivity.class);
        intent.putExtra(SKU_SUMMARY_KEY, Parcels.wrap(skuSummary));
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

         if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            SkuSummary skuSummary = (SkuSummary) Parcels.unwrap(getIntent().getParcelableExtra(SKU_SUMMARY_KEY));

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.transaction_detail_container,
                            TransactionDetailFragment.newInstance(skuSummary))
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            navigateUpTo(new Intent(this, SkuListActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
