package com.guidovezzoni.viewyourtransactions.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.guidovezzoni.viewyourtransactions.R;
import com.guidovezzoni.viewyourtransactions.adapter.SkuAdapter;
import com.guidovezzoni.viewyourtransactions.interfaces.SkuAdapterListener;
import com.guidovezzoni.viewyourtransactions.models.data.SkuSummary;

import java.util.ArrayList;
import java.util.List;

public class SkuListActivity extends AppCompatActivity implements SkuListView {
    private SkuAdapter mSkuAdapter;
    private RecyclerView mRecyclerView;

    private List<SkuSummary> mTransactions;

    private SkuListPresenter mSkuListPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sku_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        mRecyclerView = (RecyclerView) findViewById(R.id.transaction_list);

        // create an empty list initially
        mTransactions = new ArrayList<>();
        mSkuAdapter = new SkuAdapter(mTransactions, new SkuAdapterListener() {
            @Override
            public void onSkuClick(int position, SkuSummary skuSummary) {
                mSkuListPresenter.onItemClicked(position, skuSummary);
            }
        });
        mRecyclerView.setAdapter(mSkuAdapter);

        mSkuListPresenter = new SkuListPresenterImpl(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSkuListPresenter.onResume();
    }

    @Override
    protected void onDestroy() {
        mSkuListPresenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void setList(List<SkuSummary> skus) {
        mTransactions.clear();
        mTransactions.addAll(skus);
        mSkuAdapter.notifyDataSetChanged();
    }

    @Override
    public void openDetailView(SkuSummary skuSummary) {
        startActivity(TransactionDetailActivity.getNewIntent(SkuListActivity.this, skuSummary));
    }

    @Override
    public Context getContext() {
        return this;
    }

}
