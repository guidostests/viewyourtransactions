package com.guidovezzoni.viewyourtransactions.activity;

import com.guidovezzoni.viewyourtransactions.models.data.SkuSummary;

/**
 * Created by guido on 13/08/16.
 */
public interface SkuListPresenter {

    void onResume();

    void onItemClicked(int position, SkuSummary skuSummary);

    void onDestroy();


}
