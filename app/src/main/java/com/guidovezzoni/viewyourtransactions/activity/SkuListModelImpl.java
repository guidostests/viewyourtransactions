package com.guidovezzoni.viewyourtransactions.activity;

import android.content.Context;
import android.os.AsyncTask;

import com.guidovezzoni.viewyourtransactions.controller.TransactionDataHolder;
import com.guidovezzoni.viewyourtransactions.models.api.RateModel;
import com.guidovezzoni.viewyourtransactions.models.api.TransactionModel;
import com.guidovezzoni.viewyourtransactions.models.data.SkuSummary;
import com.guidovezzoni.viewyourtransactions.utils.NetworkData;

import java.util.List;

/**
 * Created by guido on 13/08/16.
 */
public class SkuListModelImpl implements SkuListModel {
    private Context mContext;

    private TransactionDataHolder mTransactionDataHolder;

    public SkuListModelImpl(Context context) {
        mContext = context;
    }

    @Override
    public void retrieveSkus(OnSkusRetrievedListener listener) {

        RetrieveDataAsyncTask retrieveDataAsyncTask = new RetrieveDataAsyncTask(mContext, listener);
        retrieveDataAsyncTask.execute();
    }

    private class RetrieveDataAsyncTask extends AsyncTask< Void, Void, List<SkuSummary>> {
        private Context mContext;
        private OnSkusRetrievedListener mListener;

        public RetrieveDataAsyncTask(Context context, OnSkusRetrievedListener listener) {
            mContext = context;
            mListener = listener;
        }

        @Override
        protected List<SkuSummary> doInBackground(Void... params) {
            // these 3 operation all are potentially time consuming so they must be in an AsyncTask
            List<RateModel>  rates = NetworkData.getRates(mContext);
            List<TransactionModel> transactions = NetworkData.getTransactions(mContext);

            return new TransactionDataHolder(transactions, rates).getRecyclerList();
        }

        @Override
        protected void onPostExecute(List<SkuSummary> skuSummaries) {
            super.onPostExecute(skuSummaries);
            if (mListener != null) {
                mListener.onCompletion(skuSummaries);
            }

        }
    }


}
