package com.guidovezzoni.viewyourtransactions.activity;

import com.guidovezzoni.viewyourtransactions.models.data.SkuSummary;

import java.util.List;

/**
 * Created by guido on 13/08/16.
 */
public interface SkuListModel {

    interface OnSkusRetrievedListener {
        void onCompletion(List<SkuSummary> skuSummaries);
    }


    void retrieveSkus(OnSkusRetrievedListener listener);
}
