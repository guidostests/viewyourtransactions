package com.guidovezzoni.viewyourtransactions.activity;

import android.content.Context;

import com.guidovezzoni.viewyourtransactions.models.data.SkuSummary;

import java.util.List;

/**
 * Created by guido on 13/08/16.
 */
public interface SkuListView {
    void showProgress();

    void hideProgress();

    void setList(List<SkuSummary> skus);

    void openDetailView(SkuSummary skuSummary);

    Context getContext();
}
