package com.guidovezzoni.viewyourtransactions.utils;

import android.content.Context;
import android.support.annotation.RawRes;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.guidovezzoni.viewyourtransactions.R;
import com.guidovezzoni.viewyourtransactions.models.api.RateModel;
import com.guidovezzoni.viewyourtransactions.models.api.TransactionModel;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by guido on 30/07/16.
 */

/*
* this class returns data as if we were querying an endpoint with -say- retrofit
* */
public class NetworkData {

    public static List<RateModel> getRates(Context context) {

        Type listType = new TypeToken<ArrayList<RateModel>>() {
        }.getType();

        String json = getFile(context, R.raw.rates);
        try {
            if (!TextUtils.isEmpty(json)) {
                return new Gson().fromJson(json, listType);
            } else {
                return new ArrayList<>();
            }
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public static List<TransactionModel> getTransactions(Context context) {
        Type listType = new TypeToken<ArrayList<TransactionModel>>() {
        }.getType();

        try {
            String json = getFile(context, R.raw.transactions);
            if (!TextUtils.isEmpty(json)) {
                return new Gson().fromJson(json, listType);
            } else {
                return new ArrayList<>();
            }
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    private static String getFile(Context context, @RawRes int rawResId) {
        InputStream is = context.getResources().openRawResource(rawResId);
        String jsonString;

        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
            jsonString = writer.toString();
        } catch (Exception e) {
            jsonString = "";
        } finally {
            try {
                is.close();
            } catch (Exception e) {

            }

        }

        return jsonString;
    }
}
