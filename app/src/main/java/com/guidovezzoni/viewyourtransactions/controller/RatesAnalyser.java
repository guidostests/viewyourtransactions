package com.guidovezzoni.viewyourtransactions.controller;

import com.guidovezzoni.viewyourtransactions.algorithm.DijkstraAlgorithm;
import com.guidovezzoni.viewyourtransactions.algorithm.Graph;
import com.guidovezzoni.viewyourtransactions.algorithm.Vertex;
import com.guidovezzoni.viewyourtransactions.models.api.RateModel;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by guido on 30/07/16.
 */
public class RatesAnalyser {
    // this stores known rate conversion either from json or previously calculated
    private HashMap<String, Double> mKnownConversion = new HashMap<>();

    // this Graph is used to store the known conversion rate and find missing conversions
    private Graph mGraph = new Graph();

    public RatesAnalyser(List<RateModel> rates, boolean calculateOppositeRate) {
        parseRates(rates, calculateOppositeRate);
    }


    /*
    * parse rates and updatre data structure accordingly
    * */
    private void parseRates(List<RateModel> rates, boolean calculateOppositeRate) {
        // add direct conversions to the HashMap
        for (RateModel rateModel : rates) {
            updateDataStrucutres(rateModel);
        }

        // if reverse rate hasn't been explicitly added from json, add it now
        // this option can be prevented
        if (calculateOppositeRate) {
            for (RateModel rateModel : rates) {
                RateModel inverseRate = rateModel.getOppositeRate();
                if (!mKnownConversion.containsKey(inverseRate.getHashMapKey())) {
                    updateDataStrucutres(inverseRate);
                }
            }
        }

    }

    /**
     * simply add new conversion to HashMap and Graph
     * */
    private void updateDataStrucutres(RateModel rateModel) {
        mKnownConversion.put(rateModel.getHashMapKey(), rateModel.rate);
        mGraph.addEdge(rateModel.from, rateModel.to);
    }

    /**
     * find out the rate either from the HashMap or from the Graph
     * */
    public Double getRate(String from, String to) {

        // check if rate exists
        Double value = mKnownConversion.get(from + to);
        if (value != null) {
            return value;
        }

        DijkstraAlgorithm dijkstraAlgorithm = new DijkstraAlgorithm(mGraph);
        dijkstraAlgorithm.execute(mGraph.getVertex(from));
        LinkedList<Vertex> path = dijkstraAlgorithm.getPath(mGraph.getVertex(to));

        if (path!=null) {
            value = 1D;
            String previous = "";
            for (Vertex vertex : path) {
                // this is to exclude the first iterations
                if (!previous.isEmpty()) {
                    value *= mKnownConversion.get(previous + vertex.getId());
                }
                previous = vertex.getId();
            }

            // now value contains the final conversion rate
            // add it to the HashMap for future references
            updateDataStrucutres(new RateModel(from, to, value));

            return value;
        } else {
            return 0D;
        }
    }

}
