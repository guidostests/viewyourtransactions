package com.guidovezzoni.viewyourtransactions.controller;

import android.support.annotation.NonNull;

import com.guidovezzoni.viewyourtransactions.models.api.RateModel;
import com.guidovezzoni.viewyourtransactions.models.api.TransactionModel;
import com.guidovezzoni.viewyourtransactions.models.data.SkuSummary;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Created by guido on 30/07/16.
 * <p/>
 * TransactionDataHolder handles a list of transaction and performs all related operations
 */
public class TransactionDataHolder {
    private HashMap<String, SkuSummary> mTransactionsHashMap;
    private RatesAnalyser mRatesAnalyser;

    public TransactionDataHolder(@NonNull List<TransactionModel> transactions, @NonNull List<RateModel> rates) {
        mRatesAnalyser = new RatesAnalyser(rates, true);
        mTransactionsHashMap = parseTransactions(transactions);
    }

    /*
    * parses all transactions and groups them in the hashmap using SKU as key
    * */
    private HashMap<String, SkuSummary> parseTransactions(List<TransactionModel> transactions) {
        HashMap<String, SkuSummary> hashMap = new HashMap<>();

        // creates hashmap entries and groups transactions
        for (TransactionModel jsonTransactions : transactions) {

            // look for the sku in the hashmap
            SkuSummary summary = hashMap.get(jsonTransactions.sku);
            if (summary == null) {
                // sku doesn't exist, creates an entry
                hashMap.put(jsonTransactions.sku, new SkuSummary(jsonTransactions.sku, jsonTransactions));
            } else {
                // sku exists, add the transaction
                summary.addTransaction(jsonTransactions);
            }
        }

        // converts and sums
        Iterator it = hashMap.entrySet().iterator();
        while (it.hasNext()) {
            HashMap.Entry pair = (HashMap.Entry) it.next();

            ((SkuSummary) pair.getValue()).convertsAndAddup(mRatesAnalyser);
        }

        return hashMap;
    }


    public HashMap<String, SkuSummary> getTransactions() {
        return mTransactionsHashMap;
    }


    public List<SkuSummary> getRecyclerList() {
        return internalGetRecyclerList(mTransactionsHashMap);
    }

    /* as recycler adapter works much better with a list, this method returns it
     * It will be shown in the main activity
     */
    private List<SkuSummary> internalGetRecyclerList(HashMap<String, SkuSummary> transactionSummary) {
        List<SkuSummary> summaryList = new ArrayList<>();

        Iterator it = transactionSummary.entrySet().iterator();
        while (it.hasNext()) {
            HashMap.Entry pair = (HashMap.Entry) it.next();

            summaryList.add(((SkuSummary) pair.getValue()));
        }

        // alpha sorting
        Collections.sort(summaryList, new Comparator<SkuSummary>() {
            public int compare(SkuSummary v1, SkuSummary v2) {
                return v1.mSku.compareTo(v2.mSku);
            }
        });

        return  summaryList;
    }

}
