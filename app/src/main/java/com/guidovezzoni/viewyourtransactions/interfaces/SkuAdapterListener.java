package com.guidovezzoni.viewyourtransactions.interfaces;

import com.guidovezzoni.viewyourtransactions.models.data.SkuSummary;

/**
 * Created by guido on 30/07/16.
 */
public interface SkuAdapterListener {
    void onSkuClick(int position, SkuSummary skuSummary);
}
