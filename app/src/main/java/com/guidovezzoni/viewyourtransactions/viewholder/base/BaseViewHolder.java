package com.guidovezzoni.viewyourtransactions.viewholder.base;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by guido on 30/07/16.
 */
public abstract class BaseViewHolder extends RecyclerView.ViewHolder {
    protected final View mView;
    protected final Context mContext;


    public BaseViewHolder(View itemView) {
        super(itemView);
        mView = itemView;
        mContext = mView.getContext();
    }

    public abstract void onBindViewHolder(Object item, int position);

}
