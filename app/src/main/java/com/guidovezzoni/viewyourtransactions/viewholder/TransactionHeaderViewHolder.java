package com.guidovezzoni.viewyourtransactions.viewholder;

import android.view.View;
import android.widget.TextView;

import com.guidovezzoni.viewyourtransactions.R;
import com.guidovezzoni.viewyourtransactions.viewholder.base.BaseViewHolder;

import java.text.NumberFormat;

/**
 * Created by guido on 30/07/16.
 */
public class TransactionHeaderViewHolder extends BaseViewHolder {
    public final TextView mSkuName;

    public TransactionHeaderViewHolder(View view) {
        super(view);
        mSkuName = (TextView) view.findViewById(R.id.sku_name);
    }

    @Override
    public void onBindViewHolder(Object item, final int position) {
        final Double total= (Double) item;

        NumberFormat defaultFormat = NumberFormat.getCurrencyInstance();
        mSkuName.setText(defaultFormat.format(total));
    }

}
