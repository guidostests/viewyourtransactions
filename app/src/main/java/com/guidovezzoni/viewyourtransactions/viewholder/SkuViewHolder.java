package com.guidovezzoni.viewyourtransactions.viewholder;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.guidovezzoni.viewyourtransactions.R;
import com.guidovezzoni.viewyourtransactions.interfaces.SkuAdapterListener;
import com.guidovezzoni.viewyourtransactions.models.data.SkuSummary;
import com.guidovezzoni.viewyourtransactions.viewholder.base.BaseViewHolder;

/**
 * Created by guido on 30/07/16.
 */
public class SkuViewHolder extends BaseViewHolder {
    public final TextView mSkuName;
    public final TextView mSkuDescription;
    public final SkuAdapterListener mListener;

    public SkuViewHolder(View view, @NonNull SkuAdapterListener listener) {
        super(view);
        mSkuName = (TextView) view.findViewById(R.id.sku_name);
        mSkuDescription = (TextView) view.findViewById(R.id.sku_description);
        mListener = listener;
    }

    @Override
    public void onBindViewHolder(Object item, final int position) {
        final SkuSummary skuSummary = (SkuSummary) item;
        final int transNo = skuSummary.getTransactionCount();

        if (skuSummary.mRateZeroFlag){
            mView.setBackgroundColor(mContext.getResources().getColor(R.color.colorAccent));
        } else {
            mView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
        }

        mSkuName.setText( skuSummary.mSku);
        mSkuDescription.setText(mContext.getResources().getQuantityString(R.plurals.transaction_name, transNo, transNo));

        mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onSkuClick(position, skuSummary);
            }
        });
    }
}
