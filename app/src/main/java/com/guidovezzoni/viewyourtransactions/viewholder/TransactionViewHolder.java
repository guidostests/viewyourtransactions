package com.guidovezzoni.viewyourtransactions.viewholder;

import android.view.View;
import android.widget.TextView;

import com.guidovezzoni.viewyourtransactions.R;
import com.guidovezzoni.viewyourtransactions.models.api.TransactionModel;
import com.guidovezzoni.viewyourtransactions.viewholder.base.BaseViewHolder;

import java.text.NumberFormat;

/**
 * Created by guido on 30/07/16.
 */
public class TransactionViewHolder extends BaseViewHolder {
    public final TextView mSkuName;
    public final TextView mSkuDescription;

    public TransactionViewHolder(View view) {
        super(view);
        mSkuName = (TextView) view.findViewById(R.id.sku_name);
        mSkuDescription = (TextView) view.findViewById(R.id.sku_description);
    }

    @Override
    public void onBindViewHolder(Object item, final int position) {
        final TransactionModel transactionModel = (TransactionModel) item;
        NumberFormat defaultFormat = NumberFormat.getCurrencyInstance();

        if (transactionModel.mRateZeroFlag){
            mView.setBackgroundColor(mContext.getResources().getColor(R.color.colorAccent));
        } else {
            mView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
        }

        // TODO formatting foreign currency
        mSkuName.setText( transactionModel.currency+" "+transactionModel.amount);
        mSkuDescription.setText(defaultFormat.format(transactionModel.mAmountGbp));
    }

}
