package com.guidovezzoni.viewyourtransactions.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.guidovezzoni.viewyourtransactions.R;
import com.guidovezzoni.viewyourtransactions.models.api.TransactionModel;
import com.guidovezzoni.viewyourtransactions.models.data.SkuSummary;
import com.guidovezzoni.viewyourtransactions.viewholder.TransactionHeaderViewHolder;
import com.guidovezzoni.viewyourtransactions.viewholder.TransactionViewHolder;
import com.guidovezzoni.viewyourtransactions.viewholder.base.BaseViewHolder;

/**
 * Created by guido on 30/07/16.
 */
public class TransactionsAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    private static final int ITEM_TYPE_HEADER = 0;
    private static final int ITEM_TYPE_TRANSACTION = 1;

    private SkuSummary mSkuSummary;

    public TransactionsAdapter(SkuSummary skuSummary) {
        mSkuSummary = skuSummary;
    }

    /**
     * first item will be a header, teh others will be transactions
     */
    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return ITEM_TYPE_HEADER;
        } else {
            return ITEM_TYPE_TRANSACTION;
        }
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM_TYPE_HEADER) {
            View view = LayoutInflater
                    .from(parent.getContext())
                    .inflate(R.layout.item_sku_header, parent, false);

            return new TransactionHeaderViewHolder(view);

        } else {
            View view = LayoutInflater
                    .from(parent.getContext())
                    .inflate(R.layout.item_sku, parent, false);

            return new TransactionViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(final BaseViewHolder holder, int position) {
        if (holder instanceof TransactionViewHolder) {
            TransactionModel transactionModel = mSkuSummary.mTransactions.get(position - 1);
            holder.onBindViewHolder(transactionModel, position);
        } else if(holder instanceof TransactionHeaderViewHolder){
            holder.onBindViewHolder(mSkuSummary.mAmountGbp, position);
        } else {
            throw new IllegalStateException("Unknown item view type");
        }


    }

    @Override
    public int getItemCount() {
        return mSkuSummary.mTransactions.size() + 1;
    }
}
