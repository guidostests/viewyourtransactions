package com.guidovezzoni.viewyourtransactions.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.guidovezzoni.viewyourtransactions.R;
import com.guidovezzoni.viewyourtransactions.interfaces.SkuAdapterListener;
import com.guidovezzoni.viewyourtransactions.models.data.SkuSummary;
import com.guidovezzoni.viewyourtransactions.viewholder.SkuViewHolder;

import java.util.List;

/**
 * Created by guido on 30/07/16.
 */
public class SkuAdapter extends RecyclerView.Adapter<SkuViewHolder> {
    private SkuAdapterListener mListener;

    private List<SkuSummary> mList;


    public SkuAdapter(List<SkuSummary> list, SkuAdapterListener listener) {
        mListener = listener;
        mList = list;
    }

    @Override
    public SkuViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_sku, parent, false);

        return new SkuViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(final SkuViewHolder holder, int position) {
        SkuSummary skuSummary = mList.get(position);

        holder.onBindViewHolder(skuSummary, position);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

}
