package com.guidovezzoni.viewyourtransactions.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.guidovezzoni.viewyourtransactions.R;
import com.guidovezzoni.viewyourtransactions.adapter.TransactionsAdapter;
import com.guidovezzoni.viewyourtransactions.models.data.SkuSummary;

import org.parceler.Parcels;

public class TransactionDetailFragment extends Fragment {
    private static final String SKU_SUMMARY_KEY = "SKU_SUMMARY";

    private SkuSummary mSkuSummary=null;

    public static TransactionDetailFragment newInstance(SkuSummary skuSummary) {
        TransactionDetailFragment fragment = new TransactionDetailFragment();

        Bundle args = new Bundle();
        args.putParcelable(SKU_SUMMARY_KEY, Parcels.wrap(skuSummary));
        fragment.setArguments(args);

        return fragment;
    }

    public TransactionDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(SKU_SUMMARY_KEY)) {
            mSkuSummary = (SkuSummary) Parcels.unwrap(getArguments().getParcelable(SKU_SUMMARY_KEY));

            getActivity().setTitle("Transaction for "+mSkuSummary.mSku);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_transaction_detail, container, false);

        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.transactions_list);
        recyclerView.setAdapter(new TransactionsAdapter(mSkuSummary));

        return rootView;
    }
}
