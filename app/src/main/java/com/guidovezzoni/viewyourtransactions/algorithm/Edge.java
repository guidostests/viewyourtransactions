package com.guidovezzoni.viewyourtransactions.algorithm;

/**
 * Created by guido on 30/07/16.
 */
public class Edge {
    private final String mId;
    private final Vertex mSource;
    private final Vertex mDestination;
    private final int mWeight;

    public Edge(Vertex source, Vertex destination) {
        this(source.toString() + destination.toString(), source, destination, 1);
    }

    public Edge(String id, Vertex source, Vertex destination, int weight) {
        mId = id;
        mSource = source;
        mDestination = destination;
        mWeight = weight;
    }

    public String getId() {
        return mId;
    }

    public Vertex getmDestination() {
        return mDestination;
    }

    public Vertex getmSource() {
        return mSource;
    }

    public int getWeight() {
        return mWeight;
    }

    @Override
    public String toString() {
        return mSource + " " + mDestination;
    }


}
