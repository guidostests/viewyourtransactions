package com.guidovezzoni.viewyourtransactions.algorithm;

/**
 * Created by guido on 30/07/16.
 */
public class Vertex {
    private final String mId;

    public Vertex(String id) {
        mId = id;
    }

    @Deprecated
    public Vertex(String id, String name) {
        mId = id;
    }

    public String getId() {
        return mId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((mId == null) ? 0 : mId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Vertex other = (Vertex) obj;
        if (mId == null) {
            if (other.mId != null)
                return false;
        } else if (!mId.equals(other.mId))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return mId;
    }

}
