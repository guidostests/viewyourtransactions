package com.guidovezzoni.viewyourtransactions.algorithm;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by guido on 30/07/16.
 */
public class Graph {
    private final List<Vertex> mVertexes;
    private final List<Edge> mEdges;

    public Graph() {
        mVertexes = new ArrayList<>();
        mEdges = new ArrayList<>();
    }

    public Graph(List<Vertex> vertexes, List<Edge> edges) {
        mVertexes = vertexes;
        mEdges = edges;
    }

    public List<Vertex> getVertexes() {
        return mVertexes;
    }

    public List<Edge> getEdges() {
        return mEdges;
    }

    public void addEdge(Vertex source, Vertex destination) {
        // ensure Vertexes are listed already
        if (!mVertexes.contains(source)) {
            mVertexes.add(source);
        }
        if (!mVertexes.contains(destination)) {
            mVertexes.add(destination);
        }

        // add the edge
        mEdges.add(new Edge(source, destination));
    }

    public void addEdge(String source, String destination) {
        addEdge(new Vertex(source), new Vertex(destination));
    }

    public Vertex getVertex(String vertexId) {
        // search for the index of a givenvertexId
        int index = mVertexes.indexOf(new Vertex(vertexId));
        if (index==-1) {
            return null;
        }else {
            //if present return it
            return mVertexes.get(index);
        }
    }

}
